## TODO

### Sreejesh
- [ ] Create JSON dummy data
- [ ] Create database models and save data in it
- [ ] Create a service to download json data and put in a list of CollectionData

### Vishwa
- [x] Create users view
- [x] Remove buttons which cause cycles
- [ ] Add dark and light mode

### Doubts
- [ ] SampleDataService class can be made static 
- [ ] 2 New services, webservice and dbservice respectively. Dataservice calls both
- [ ] {} in newLine
- [ ] 