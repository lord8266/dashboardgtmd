import zipfile
import os
import sys
import io
import requests
from pprint import pprint

import json
if len(sys.argv)!=2 or not os.path.isdir(sys.argv[1]):
    print("Usage:\npython3 add.py <path to directory containing json files>")
    exit(0)

path = sys.argv[1]

def make_zfile():
    files = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path,f))  ]
    mem = io.BytesIO()
    zfile = zipfile.ZipFile(mem,mode='w')
    for f in files:
        p = os.path.join(path,f)
        print("Adding",p)
        zfile.write(p,f)
    zfile.close()
    mem.seek(0)
    return mem

def send_data(data):
    url = 'https://9ll02q19ai.execute-api.ap-south-1.amazonaws.com/default/AddDashboardFiles'
    print("Making request to",url)
    res = requests.post(url,data=data)
    print("Received",res.content.decode())
    if res.status_code==200:
        print("Done")
    else:
        print("Error")
        
zfile = make_zfile()
send_data(zfile)
