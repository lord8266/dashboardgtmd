﻿using Microcharts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SkiaSharp;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace DashboardGTMD
{
    public class CollectionData
    {
        public CollectionData(string userId, int id, string name, float target, float achieved, int year, int month)
        {
            UserId = userId;
            Id = id;
            Name = name;
            Target = target;
            Achieved = achieved;
            Year = year;
            Month = month;
        }

        public string UserId { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public float Target { get; set; }
        public float Achieved { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
    }

    public class YearlyCollectionData

    {
        public YearlyCollectionData()
        {
        }

        public YearlyCollectionData(List<CollectionData> colData)
        {
            ColData = colData;
            YearlyAchieved = YearlyTarget = 0;
            colData.ForEach(v =>
            {
                YearlyTarget += v.Target;
                YearlyAchieved += v.Achieved;
            });
            ColData.Sort((c1, c2) => c1.Month - c2.Month);
            Year = colData[0].Year;
        }

        public List<CollectionData> ColData { get; set; }
        public float YearlyAchieved { get; set; }
        public float YearlyTarget { get; set; }
        public int Year { get; set; }
    }

    public class CategoryGroupWithChart
    {
        public CategoryGroupWithChart(IList<CollectionData> data)
        {
            Data = data.ToList();
            Achieved = Target = 0;
            foreach(var c in data)
            {
                Achieved += c.Achieved;
                Target += c.Target;
            }
            Year = data[0].Year;
            Data.Sort((c1, c2) => c1.Month - c2.Month);
        }

        public float Achieved { get; set; }
        public float Target { get; set; }
        public List<CollectionData> Data { get; set; }
        public LineChart Chart { get; set; }
        public int Year { get; set; }

    }
}
