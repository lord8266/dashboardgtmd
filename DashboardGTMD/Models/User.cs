﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {

    
    public class User {
        public User(string id, string name) {
            Id = id;
            Name = name;
        }
        public string Id { get; set; }

        
        public string Name { get; set; }
    }
}
