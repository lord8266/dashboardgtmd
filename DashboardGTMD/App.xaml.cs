﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DashboardGTMD
{
    public partial class App : Application
    {
        public static App Instance;
        public event EventHandler ColorChangedEvent;
        public OSAppTheme CurrentOSTheme {
            get => UserAppTheme;
            set {
                if (value == OSAppTheme.Unspecified) {
                    value = OSAppTheme.Dark;
                }
                UserAppTheme = value;
                ColorChangedEvent?.Invoke(this, null);
                SetImageSource();
            }
        }
        public App() {
            Instance = this;
            Task.Run(async () => {
                await DbService.DbInit();
            });
            
            InitializeComponent();
            CurrentOSTheme = UserAppTheme;
            Current.RequestedThemeChanged += (s, e) => {
                CurrentOSTheme = e.RequestedTheme;
            };
            MainPage = new NavigationPage(new UserSelect());

        }

        void SetImageSource() {
            if (UserAppTheme == OSAppTheme.Light) {
                Resources["expand"] = "expand.png";
                Resources["collapse"] = "collapse.png";
            } else {
                Resources["expand"] = "expandw.png";
                Resources["collapse"] = "collapsew.png";
            }
        }

        protected override void OnStart() {
            
        }

        protected override void OnSleep() {
        }

        protected override void OnResume() {
        }
    }
}
