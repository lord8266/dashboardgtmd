﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DashboardGTMD {
    class MonthViewViewModel : INotifyPropertyChanged {
        public event PropertyChangedEventHandler PropertyChanged;
        public CategoryGroupWithChart Data { get; set; }
        public string DateString { get; set; }
        public bool ViewAllEnabled { get; set; }

        public LineChart Chart { get; set; }
        public MonthViewViewModel(CategoryGroupWithChart data, bool viewAllEnabled) {
            Data = data;
            DateString = String.Format("Year {0}",data.Data[0].Year);
            ViewAllEnabled = viewAllEnabled;
            Load();
            App.Instance.ColorChangedEvent += (s, e) => {
                Load();
            };

        }

        async void Load() {

            Data.Chart = makeChart(Data.Data);
            CallPropertyChanged(nameof(Data));
        }

        void CallPropertyChanged(string name) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        LineChart makeChart(List<CollectionData> data) {
            List<ChartEntry> entryA = new List<ChartEntry>();
            List<ChartEntry> entryT = new List<ChartEntry>();
            float a = 0, t = 0;
            data.ForEach(c => {
                a += c.Achieved;
                t += c.Target;
                string monthName = (new System.Globalization.DateTimeFormatInfo()).GetAbbreviatedMonthName(c.Month);
                var col = t <= a ? Colors.Green : Colors.Red;
                entryA.Add(new ChartEntry(a) { Color = col, Label = monthName, ValueLabel = a.ToString(), ValueLabelColor = Theme.CurrentTheme.ChartTextColor });
                entryT.Add(new ChartEntry(t) { Color = Theme.CurrentTheme.ChartTargetLine, ValueLabel = t.ToString(), ValueLabelColor = Theme.CurrentTheme.ChartTextColor });
                
            });

            var chart = new RightZeroMarginLineChart() {
                Series = new ChartSerie[] {
                    new ChartSerie(){Entries=entryA,Color=null},
                    new ChartSerie(){Entries=entryT,Color=null }
                },
                LineMode = LineMode.Straight,
                LineSize=10,
                PointMode = PointMode.Circle,
                BackgroundColor = Theme.CurrentTheme.ChartBackground,
                LabelTextSize = 30f,
                LabelColor= Theme.CurrentTheme.ChartTextColor,
                LabelOrientation=Orientation.Horizontal,
                ValueLabelOrientation = Orientation.Horizontal,
                ShowYAxisText = true,
                YAxisLinesPaint = Theme.CurrentTheme.ChartYAxisPaint,
                ShowYAxisLines = true,
                YAxisMaxTicks=5,
                YAxisTextPaint = Theme.CurrentTheme.ChartYAxisPaint,
                YAxisPosition = Position.Left,
                ValueLabelOption =ValueLabelOption.None,
                AnimationDuration= TimeSpan.FromSeconds(0.5f),
                LineAreaAlpha =15
            };

            //ValueLabelOption = ValueLabelOption.TopOfChart,
            //    PointMode = PointMode.None,
            //    Series = new ChartSerie[] { new ChartSerie() { Entries = entries, Color = C1 }, new ChartSerie() { Entries = entries2, Color = C2 } },
            //    ShowYAxisText = true,
            //    ShowYAxisLines = true,
            //    YAxisPosition = Position.Left,
            //    BackgroundColor = SKColor.Parse("#1a1a1a"), 
            //    LineMode = LineMode.Straight, 
            //    LabelTextSize = 40f,
            //    ValueLabelTextSize = 40f,
            //    ValueLabelOrientation = Orientation.Horizontal,
            //    YAxisTextPaint = new SKPaint
            //    {
            //        TextSize = 40.0f,
            //        IsAntialias = true,
            //        Color = new SKColor(255, 0, 0),
            //        Style = SKPaintStyle.Fill
            //    },
            return chart;
        }
    }
}
