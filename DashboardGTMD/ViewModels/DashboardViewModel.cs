﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace DashboardGTMD {
    class DashboardViewModel : INotifyPropertyChanged {
        #region Members
        YearlyCollectionData current;
        ObservableCollection<CategoryGroupWithChart> currentGroup;
        Chart currentPie;
        ObservableCollection<YearlyCollectionData> dataList;
        PageState pageState = PageState.LOADING;
        ListState listState = ListState.EMPTY;
        ObservableCollection<CategoryHistory> historyList;
        #endregion
        public PageState PageState { get => pageState; set { pageState = value; CallPropertyChanged(nameof(PageState)); } }
        public ListState ListState { get => listState; set { listState = value; CallPropertyChanged(nameof(ListState)); } }
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<YearlyCollectionData> DataList { get => dataList; set { dataList = value; CallPropertyChanged(nameof(DataList)); } }
        public ObservableCollection<CategoryHistory> HistoryList { get => historyList; set { historyList = value; CallPropertyChanged(nameof(HistoryList)); } }
        public YearlyCollectionData Current {
            get {
                return current;
            }
            set {
                if (value == null) {
                    return;
                }
                current = value;
                CallPropertyChanged(nameof(Current));
                CurrentGroup = new ObservableCollection<CategoryGroupWithChart>(
                    Gatherer.GroupByCategory(Current)
                    .Select(c => {
                        var cg = new CategoryGroupWithChart(c);
                        cg.Chart = MakeChart(cg.Data);
                        return cg;
                    }));
                CurrentCategoryPie = new PieChart() {
                    Entries = current.ColData.GroupBy(g => g.Id).Select((gi, index) => {
                        var s = gi.Select(s => s.Achieved).Sum();
                        return new ChartEntry(s) {
                            
                            Label = gi.First().Name,
                            ValueLabel = s.ToString(),
                            ValueLabelColor = Theme.CurrentTheme.ChartTextColor,
                            Color = Theme.CurrentTheme.PieColors[index],
                            TextColor = Theme.CurrentTheme.ChartTextColor
                        };
                    }),
                    LabelMode=LabelMode.RightOnly,
                    BackgroundColor = SKColor.Parse("#00000000"),
                    LabelTextSize = 35.0f,
                    LabelColor = Theme.CurrentTheme.ChartTextColor
                };
            }
        }

        public ObservableCollection<CategoryGroupWithChart> CurrentGroup {
            get {
                return currentGroup;
            }
            set {
                currentGroup = value;
                CallPropertyChanged(nameof(CurrentGroup));
            }
        }
        public Chart CurrentCategoryPie {
            get {
                return currentPie;
            }
            set {
                currentPie = value;
                CallPropertyChanged(nameof(CurrentCategoryPie));
            }
        }

        public User User { get; set; }
        public DashboardViewModel(User user) {
            Console.WriteLine("Conbsturctor {0}", Thread.CurrentThread.ManagedThreadId);
            User = user;
            Load();
            App.Instance.ColorChangedEvent += (s, e) => {
                Load();
            };
        }

        public async void Load() {
            PageState = PageState.LOADING;
            var coldata = await Defaults.CachingDataService.GetAllDataForUser(User);
            if (coldata == null) {
                PageState = PageState.ERROR;
                return;
            } else if (coldata.Count == 0) {
                PageState = PageState.LOADED;
            } else {
                ListState = ListState.NONEMPTY;
                PageState = PageState.LOADED;
            }
            GlobalState.CurrentUserData = coldata;
            var cl = new ObservableCollection<YearlyCollectionData>(Gatherer.GroupByYear(coldata));
            var hl = MakeCategoryHistory(coldata);
            Device.BeginInvokeOnMainThread(() => {
                DataList = cl;
                if (DataList.Count > 0) {
                    Console.WriteLine("Current {0}", Thread.CurrentThread.ManagedThreadId);
                    Current = DataList[DataList.Count - 1];
                }
                HistoryList = hl;
            });
        }

        ObservableCollection<CategoryHistory> MakeCategoryHistory(List<CollectionData> data) {

            var l = Gatherer.GroupByCategory(data);
            List<CategoryHistory> hList = new List<CategoryHistory>();
            foreach (var group in l) {
                CategoryHistory ch = new CategoryHistory { Name = group[0].Name };
                var yeargroups = group.ToList().GroupBy(g => g.Year).Select((k) => {
                    return new {
                        Year = k.Key,
                        Achieved = k.Select(i => i.Achieved).Sum(),
                        Target = k.Select(i => i.Target).Sum()
                    };
                }).ToList();

                ch.Chart = new BarChart {
                    BackgroundColor=Theme.CurrentTheme.ChartBackground,
                    LabelOrientation = Orientation.Horizontal,
                    ValueLabelOrientation = Orientation.Horizontal,
                    LabelTextSize = 42,
                    Margin=35,
                    ValueLabelTextSize = 40,
                    SerieLabelTextSize = 42,
                    LegendOption = SeriesLegendOption.Bottom,
                    LabelColor= Theme.CurrentTheme.ChartTextColor,
                    Series = new List<ChartSerie>()
                    {
                        new ChartSerie()
                        {
                            Name = "Target",
                            Color = SKColor.Parse("#2c3e50"),
                            Entries = yeargroups.Select(v=>new ChartEntry(v.Target){
                                TextColor=Theme.CurrentTheme.ChartTextColor,
                                Label=v.Year.ToString(),
                                ValueLabelColor=Theme.CurrentTheme.ChartTextColor,
                                ValueLabel=v.Target.ToString()
                            }).ToList(),
                        },
                        new ChartSerie()
                        {
                            Name = "Achieved",
                            Color =  SKColor.Parse("#77d065"),
                            Entries = yeargroups.Select(v=>new ChartEntry(v.Achieved){
                                TextColor=Theme.CurrentTheme.ChartTextColor,
                                ValueLabelColor=Theme.CurrentTheme.ChartTextColor,
                                Label=v.Year.ToString(),
                                ValueLabel=v.Achieved.ToString()
                            }).ToList(),
                        }
                    }
                };
                hList.Add(ch);
            }
            Console.WriteLine("HsitorlIsit {0}", Thread.CurrentThread.ManagedThreadId);
            return new ObservableCollection<CategoryHistory>(hList);
        }

        LineChart MakeChart(IList<CollectionData> data) {
            List<ChartEntry> entryA = new List<ChartEntry>();
            List<ChartEntry> entryT = new List<ChartEntry>();
            float a = 0, t = 0;
            data.ToList().ForEach(c => {
                a += c.Achieved;
                t += c.Target;
                string monthName = (new System.Globalization.DateTimeFormatInfo()).GetAbbreviatedMonthName(c.Month);
                var col = t <= a ? Colors.Green : Colors.Red;
                entryA.Add(new ChartEntry(a) { Color = col });
                entryT.Add(new ChartEntry(t) { Color = Theme.CurrentTheme.ChartTargetLine, });
            });

            var chart = new LineChart() {
                Series = new ChartSerie[] {
                    new ChartSerie(){Entries=entryA,Color=null},
                    new ChartSerie(){Entries=entryT,Color=null }
                },
                LineMode = LineMode.Straight,
                PointMode = PointMode.Circle,
                PointSize = 6,
                BackgroundColor = Theme.CurrentTheme.ChartBackground
            };
            return chart;
        }

        void CallPropertyChanged(string name) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    class CategoryHistory {
        public string Name { get; set; }
        public BarChart Chart { get; set; }
    }

    public class CollectionYearStringConverter : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return null;
            }
            CategoryGroupWithChart cData = (CategoryGroupWithChart)value;
            return String.Format("{0} - {1}", cData.Data[0].Name, cData.Data[0].Year);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException("hmm");
        }
    }

    public class YearDataToYear : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return null;
            }
            IList<YearlyCollectionData> y = (IList<YearlyCollectionData>)value;
            return new ObservableCollection<string>(y.ToList().Select(v => v.ColData[0].Year.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException("hmm");
        }
    }
}
