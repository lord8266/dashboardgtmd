﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DashboardGTMD {
    class UserSelectViewModel : INotifyPropertyChanged {
        #region Members
        ObservableCollection<User> matchingUsers;
        string searchText;
        PageState pageState = PageState.LOADING;
        ListState listState = ListState.EMPTY;
        #endregion
        public PageState PageState { get => pageState; set { pageState = value; CallPropertyChanged(nameof(PageState)); } }
        public ListState ListState { get => listState; set { listState = value; CallPropertyChanged(nameof(ListState)); } }
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<User> Users { get; set; }
        public ObservableCollection<User> MatchingUsers {
            get { return matchingUsers; }
            set {
                matchingUsers = value;
                if (matchingUsers.Count == 0) {
                    ListState = ListState.EMPTY;
                    Console.WriteLine("Empty");
                }
                else {
                    ListState = ListState.NONEMPTY;
                }
                CallPropertyChanged(nameof(MatchingUsers));
            }
        }

        public string SearchText {
            get { return searchText; }
            set {
                searchText = value;
                UpdateMatchingUsers();
                CallPropertyChanged(nameof(SearchText));
            }
        }

        public UserSelectViewModel() {
            Task.Run(async () => {
                await Load();
            });
        }
        
        public async Task Load() {
            PageState = PageState.LOADING;
            List<User> users = await Defaults.CachingDataService.GetAllUsers();
            if (users==null) {
                PageState = PageState.ERROR;
            }
            else {
                PageState = PageState.LOADED;
                Users = new ObservableCollection<User>(users);
                MatchingUsers = Users;
            }
        }
        void CallPropertyChanged(string name) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        void UpdateMatchingUsers() {
            if (SearchText.Length == 0) {
                MatchingUsers = Users;
                return;
            }

            var l = Users.Where(u => {
                return u.Name.ToLower().Contains(SearchText.ToLower());
            }).ToList();

            l.Sort((c1, c2) => {
                int i1 = c1.Name.Length - SearchText.Length;
                int i2 = c2.Name.Length - SearchText.Length;
                return i1 - i2;
            });

            MatchingUsers = new ObservableCollection<User>(l);
        }
    }

    public class StringLength : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return null;
            }
            
            return ((string)value).Length;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException("hmm");
        }
    }

    //public class LoadingLoadedError :IValueConverter {
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
    //        if (value == null) {
    //            return null;
    //        }
    //        PageState s = (PageState)value;
    //        return s == PageState.LOADED;
    //    }

    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
    //        throw new NotImplementedException("hmm");
    //    }
    //}
    //public class LoadingLoadedErrorSelector : DataTemplateSelector {
    //    public DataTemplate LoadedTemplate { get; set; }

    //    public DataTemplate ErrorTemplate { get; set; }

    //    public DataTemplate LoadingTemplate { get; set; }

    //    public PageState CurrState { get; set; }

    //    protected override DataTemplate OnSelectTemplate(object item, BindableObject container) {
    //        if (CurrState == PageState.LOADING) {
    //            return LoadingTemplate;
    //        }
    //        else if (CurrState == PageState.ERROR) {
    //            return ErrorTemplate;
    //        }
    //        else {
    //            return LoadedTemplate;
    //        }
    //    }
    //}

    //public class EmptyNonEmpty : DataTemplateSelector {
    //    public DataTemplate Empty { get; set; }

    //    public DataTemplate NonEmpty { get; set; }

    //    public PageState CurrState { get; set; }

    //    protected override DataTemplate OnSelectTemplate(object item, BindableObject container) {
    //        if (CurrState == PageState.LOADED) {
    //            return em;
    //        } else if (CurrState == PageState.ERROR) {
    //            return ErrorTemplate;
    //        } else {
    //            return LoadedTemplate;
    //        }
    //    }
    //}
}
