﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace DashboardGTMD {
    class CategoryViewViewModel {
        public ObservableCollection<YearlyCollectionData> DataList { get; set; }
        public string CategoryName { get; set; }
        public string CategoryUnit { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
        public CategoryViewViewModel(int id) {
            DataList = new ObservableCollection<YearlyCollectionData>(
                GlobalState.CurrentUserData.Where(c => c.Id == id).GroupBy(c => c.Year, (k, v) => new YearlyCollectionData(v.ToList())));
            CategoryName = DataList[0].ColData[0].Name;
            CategoryUnit = Defaults.UnitRegex.Matches(DataList[0].ColData[0].Name).First().Groups[0].Value.ToLower();
        }

        void CallPropertyChanged(string name) {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }

    public class MonthNumToName : IValueConverter {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) {
            if (value == null) {
                return null;
            }
            string monthName = (new System.Globalization.DateTimeFormatInfo()).GetMonthName((int)value);
            return monthName;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) {
            throw new NotImplementedException("hmm");
        }
    }
}
