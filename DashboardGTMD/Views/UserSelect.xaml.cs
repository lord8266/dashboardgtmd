﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DashboardGTMD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserSelect : ToolbarPage
    {
        public UserSelect()
        {
            
            BindingContext = new UserSelectViewModel();
            InitializeComponent();
            //HahaMeme();
        }
        
        async void HahaMeme() {
            while (true) {
                await Task.Delay(5000);
                await ((UserSelectViewModel)BindingContext).Load();
            }
        }
        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            var user = (User)args.Item;
            await Navigation.PushAsync(new Dashboard(user));
        }

        async void OnClickReload(object sender, EventArgs args) {
            await ((UserSelectViewModel)BindingContext).Load();
        }
    }
}