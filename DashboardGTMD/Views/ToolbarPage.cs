﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace DashboardGTMD {
    public class ToolbarPage : ContentPage {
        public ToolbarPage() {
            LoadToolbar();

            App.Instance.ColorChangedEvent += (s, e) => {
                LoadToolbar();
            };

        }
        void LoadToolbar() {
            var item = new ToolbarItem {
                Order = ToolbarItemOrder.Secondary,
                Text = App.Instance.CurrentOSTheme == OSAppTheme.Dark ? "Enable Light Mode" : "Enable Dark Mode"
            };
            item.Clicked += OnColorModeClick;
            Task.Delay(100).ContinueWith((t) => {
                Device.BeginInvokeOnMainThread(() => {
                    ToolbarItems.Clear();
                    ToolbarItems.Add(item);
                });
            });

        }
        public void OnColorModeClick(object sender, EventArgs e) {
            ToolbarItem ti = (ToolbarItem)sender;
            if (ti.Text.Contains("Dark")) {
                App.Instance.CurrentOSTheme = OSAppTheme.Dark;
                //Task.Delay(100).ContinueWith((t) =>
                //{
                //    Device.BeginInvokeOnMainThread(() =>
                //    {
                //        ti.Text = "Enable Light Mode";
                //    });
                //});
            } else {
                App.Instance.CurrentOSTheme = OSAppTheme.Light;
                Task.Delay(100).ContinueWith((t) => {
                    Device.BeginInvokeOnMainThread(() => {
                        ti.Text = "Enable Dark Mode";
                    });
                });
            }
        }
    }
}
