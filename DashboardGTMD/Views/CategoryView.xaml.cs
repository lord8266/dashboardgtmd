﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.CommunityToolkit;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DashboardGTMD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CategoryView : ToolbarPage {
        public CategoryView(int id)
        {
            BindingContext = new CategoryViewViewModel(id);
            InitializeComponent();
        }
        async void OnItemTapped(object sender, ItemTappedEventArgs args)
        {
            YearlyCollectionData data = ((CategoryViewViewModel)BindingContext).DataList[args.ItemIndex];
            await Navigation.PushAsync(new MonthView(
                new CategoryGroupWithChart(data.ColData),false));
        }

    }
}