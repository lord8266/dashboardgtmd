﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DashboardGTMD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Dashboard : ToolbarPage {
        public Dashboard(User user)
        {
            
            InitializeComponent();
            BindingContext = new DashboardViewModel(user);
        }
        async void OnItemTapped(object sender,EventArgs args)
        {
            Frame frame = (Frame)sender;
            await frame.ScaleTo(1.01, 50);
            IList<View> children = ((StackLayout)frame.Parent).Children;
            int i = 0;
            for (; i < children.Count; i++)
            {
                if (children[i] == frame)
                {
                    break;
                }
            }

            var data = ((DashboardViewModel)BindingContext).CurrentGroup[i];
            
            await Navigation.PushAsync(new MonthView(data));
        }
        async void OnClickReload(object sender, EventArgs args) {
            ((DashboardViewModel)BindingContext).Load();
        }

    }
}