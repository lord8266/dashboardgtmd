﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DashboardGTMD
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MonthView : ToolbarPage {
        public MonthView(CategoryGroupWithChart data,bool viewAllEnabled=true)
        {
            BindingContext = new MonthViewViewModel(new CategoryGroupWithChart(data.Data),viewAllEnabled);
            
            InitializeComponent();
        }
        async void OnViewAllClick(object sender,EventArgs args)
        {
            int id = ((MonthViewViewModel)BindingContext).Data.Data[0].Id;
            await Navigation.PushAsync(new CategoryView(id));
        }


    }
}