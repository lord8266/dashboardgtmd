﻿using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    public class JSONCategory {
        [JsonPropertyNameAttribute("Sl No")]
        public int SlNo { get; set; }

        [JsonPropertyNameAttribute("Type")]
        public string Type { get; set; }

        [JsonPropertyNameAttribute("Sequence No")]
        public int SequenceNo { get; set; }
    }
}
