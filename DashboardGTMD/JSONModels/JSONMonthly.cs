﻿using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    public class JSONMonthly {
        [JsonPropertyNameAttribute("Employee Code")]
        public string EmployeeCode { get; set; }

        [JsonPropertyNameAttribute("Month")]
        public int Month { get; set; }

        [JsonPropertyNameAttribute("Year")]
        public int Year { get; set; }

        [JsonPropertyNameAttribute("Type")]
        public string Type { get; set; }

        [JsonPropertyNameAttribute("Monthly Target")]
        public float target { get; set; }
    }
}
