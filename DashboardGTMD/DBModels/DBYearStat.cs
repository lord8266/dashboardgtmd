﻿using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    [Table("year-stats")]
    public class DBYearStat {
        public string EmployeeCode { get; set; }
        public int Year { get; set; }
        public float target { get; set; }
        public float achieved { get; set; }
    }
}
