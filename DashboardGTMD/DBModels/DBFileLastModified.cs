﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    class DBFileLastModified {
        [PrimaryKey]
        public string FileURI { get; set; }
        [NotNull]
        public DateTime LastModified { get; set; }
    }
}
