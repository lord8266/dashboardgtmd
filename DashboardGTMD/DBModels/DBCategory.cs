﻿using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    [Table("categories")]
    public class DBCategory {
        
        [PrimaryKey]
        public int SlNo { get; set; }

        
        public string Type { get; set; }

        
        public int SequenceNo { get; set; }
    }
}
