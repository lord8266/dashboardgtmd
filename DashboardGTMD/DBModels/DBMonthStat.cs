﻿using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {
    [Table("month-stats")]
    public class DBMonthStat {
        public string EmployeeCode { get; set; }
        public int Month { get; set; }
        public int Year { get; set; }
        public string Type { get; set; }
        public float target { get; set; }
        public float achieved { get; set; }
    }
}
