﻿using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Text.Json.Serialization;
using SQLite;

namespace DashboardGTMD {

    [Table("users")]
    public class DBUser {
       

        
        [PrimaryKey]
        public string Id { get; set; }

        
        public string Name { get; set; }
    }
}
