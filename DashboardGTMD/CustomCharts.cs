﻿using Microcharts;
using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace DashboardGTMD
{
    class RightZeroMarginLineChart: LineChart
    {
        public override SKSize CalculateItemSize(int items, int width, int height, float reservedSpace)
        {
            var w = (width -  ((items + 1) * Margin)) / items;
            var h = height - Margin - reservedSpace;
            return new SKSize(w, h);
        }
    }
}
