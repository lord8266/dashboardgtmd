﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using SkiaSharp;
using System.Text.RegularExpressions;

namespace DashboardGTMD {
    class Defaults {

        public static List<CollectionData> SampleCollectionData = new List<CollectionData>()
        {
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",6.5f,6.5f,2020,4),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",5.5f,5.5f,2020,4),
            new CollectionData("EMP00001",4,"Vigore (In Kgs)",5.0f,5.0f,2020,4),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",8.5f,8.5f,2020,5),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",7.5f,7.5f,2020,5),
            new CollectionData("EMP00001",4,"Vigore (In Kgs)",10.0f,10.0f,2020,5),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",9.0f,9.0f,2020,6),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",8.0f,8.0f,2020,6),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",11.0f,11.0f,2020,6),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",9.5f,9.5f,2020,7),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",8.5f,8.5f,2020,7),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",12.0f,12.0f,2020,7),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",10.0f,10.0f,2020,8),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",9.0f,9.0f,2020,8),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",13.0f,13.0f,2020,8),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",10.5f,10.5f,2020,9),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",9.5f,9.5f,2020,9),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",13.0f,13.0f,2020,9),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",11.5f,11.5f,2020,10),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",10.5f,10.5f,2020,10),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",14.0f,14.0f,2020,10),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",13.0f,13.0f,2020,11),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",11.0f,11.0f,2020,11),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",15.0f,15.0f,2020,11),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",14.5f,12.0f,2020,12),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",12.5f,12.5f,2020,12),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",16.0f,16.0f,2020,12),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",16.0f,12.0f,2021,1),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",15.0f,15.0f,2021,1),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",18.0f,18.0f,2021,1),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",18.0f,18.0f,2021,2),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",17.0f,17.0f,2021,2),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",20.0f,20.0f,2021,2),
            new CollectionData("EMP00001",1,"Sales (In Lakhs)",20.0f,15.0f,2021,3),
            new CollectionData("EMP00001",2,"Collection (In Lakhs)",18.5f,18.5f,2021,3),
            new CollectionData("EMP00001",4,"Vigore (In kgs)",23.0f,23.0f,2021,3)
        };

        public static List<User> SampleUsers = new List<User>()
        {
            new User("EMP00001","Employee 1"),
            new User("EMP00002","Employee 2"),
        };

        public static Regex UnitRegex = new Regex(@"In (\w+)",RegexOptions.IgnoreCase|RegexOptions.Compiled);

        public static IDataService DataService = new SampleDataService();
        public static IDataService CachingDataService = new CachingDataService();
    }

    class Colors {
        public static SKColor Red = SKColor.Parse("#ff0000");
        public static SKColor Green = SKColor.Parse("#00ff00");
        public static SKColor White = SKColor.Parse("#ffffff");
        public static SKColor Gray = SKColor.Parse("#1a1a1a");
        public static List<SKColor> PieColors = new List<SKColor>()
        {
            SKColors.BlueViolet,SKColors.White,SKColors.Green,SKColors.Blue
        };
    }

    class SkFonts {
        public static SKPaint YAxisText = new SKPaint {
            TextSize = 25.0f,
            IsAntialias = true,
            Color = Colors.White,
            Style = SKPaintStyle.Fill
        };
        public static SKPaint YAxisLine = new SKPaint {
            TextSize = 30.0f,
            IsAntialias = true,
            Color = Colors.White,
            Style = SKPaintStyle.Stroke
        };
    }

    class JSONFile {

        static readonly string URLFormat = "https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/{0}.json";
        public static string MonthlyAchieved {
            get {
                return String.Format(URLFormat, "achieved");
            }
        }
        public static string MonthlyTarget {
            get {
                return String.Format(URLFormat, "monthlyTarget");
            }
        }

        public static string Categories {
            get {
                return String.Format(URLFormat, "categories");
            }
        }

        public static string Employees {
            get {
                return String.Format(URLFormat, "employees");
            }
        }

    };


}
