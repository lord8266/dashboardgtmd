﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace DashboardGTMD {
    class Theme {
        public static ColorTheme LightTheme = new ColorTheme {
            TextColor = Color.Black,
            BackgroundColor = Color.White,
            FrameColor = Color.WhiteSmoke,
            ChartBackground = SKColors.WhiteSmoke,
            ChartTargetLine = SKColors.Black,
            ChartTextColor = SKColors.Black,
            ChartYAxisPaint = new SKPaint {
                TextSize = 30.0f,
                IsAntialias = true,
                Color = SKColors.Black,
                Style = SKPaintStyle.Fill
            },
            PieColors = new List<SKColor> { SKColors.BlueViolet, SKColors.Green, SKColors.Brown, SKColors.OrangeRed }


        };

        public static ColorTheme DarkTheme = new ColorTheme {
            TextColor = Color.White,
            BackgroundColor = Color.Black,
            FrameColor = Color.FromHex("#1a1a1a"),
            ChartBackground = SKColor.Parse("#1a1a1a"),
            ChartTargetLine = SKColors.White,
            ChartTextColor = SKColors.White,
            ChartYAxisPaint = new SKPaint {
                TextSize = 30.0f,
                IsAntialias = true,
                Color = SKColors.White,
                Style = SKPaintStyle.Fill
            },
            PieColors = new List<SKColor> { SKColors.BlueViolet, SKColors.Green, SKColors.Brown, SKColors.OrangeRed }

        };
        public static ColorTheme CurrentTheme {
            get {
                if (Application.Current.UserAppTheme == OSAppTheme.Dark) {
                    return DarkTheme;
                } else {
                    return LightTheme;
                }
            }
        }

    }
    class ColorTheme {
        public Color TextColor { get; set; }
        public Color BackgroundColor { get; set; }
        public Color FrameColor { get; set; }
        public SKColor ChartBackground { get; set; }
        public SKColor ChartTargetLine { get; set; }
        public SKColor ChartTextColor { get; set; }
        public SKPaint ChartYAxisPaint { get; set; }
        public List<SKColor> PieColors { get; set; }
        public string ExpandImageName { get; set; }
        public string CollapseImageName { get; set; }
    }
}
