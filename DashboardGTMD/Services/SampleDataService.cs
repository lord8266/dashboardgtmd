﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using Xamarin.Essentials;
using System.Net.Http;
//using Newtonsoft.Json;
using System.Text.Json;

namespace DashboardGTMD {
    class SampleDataService : IDataService {
        private static HttpClient client = new HttpClient();

        public SampleDataService() {
            client = new HttpClient();
        }

        public async Task<List<User>> GetAllUsers() {
            Console.WriteLine("In GetAllUsers()");
            var nAccess = Connectivity.NetworkAccess;
            //if (nAccess != NetworkAccess.Internet) {
            //    return new List<User>();
            //    //return Defaults.SampleUsers;
            //} else {
            //    Uri empURL = new Uri("https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/employees.json");
            //    string response = await client.GetStringAsync(empURL);
            //    List<User> employees = JsonSerializer.Deserialize<List<User>>(response);
            //    return employees;
            //}
            return Defaults.SampleUsers;
        }

        public async Task<List<CollectionData>> GetAllDataForUser(User user)
        {
            Console.WriteLine("In GetAllDataForUser()");
            //    var nAccess = Connectivity.NetworkAccess;
            //    if (nAccess != NetworkAccess.Internet) {
            //        return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
            //    } else {
            //        return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
            //        //Uri dataURL = new Uri("https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/monthlyTarget.json");
            //        //string response = await client.GetStringAsync(dataURL);
            //        //List<MonthlyTarget> empData = JsonSerializer.Deserialize<List<MonthlyTarget>>(response);
            //        //return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
            //    }
            //}
            return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
        }
    }
}

// https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/achieved.json
// https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/categories.json
// https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/employees.json
// https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/monthlyTarget.json
// https://gtmd.s3.ap-south-1.amazonaws.com/dashboard/json/yearlyTarget.json