﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashboardGTMD {
    class CachingDataService : IDataService {


        public async Task<List<User>> GetAllUsers() {
            if (await IsUsersUpdateRequired()) {
                return await UpdateAndGetUsers();
            } else {
                return await GetLocalUsers();
            }
        }

        public async Task<List<CollectionData>> GetAllDataForUser(User user) {
            if (await IsDataUpdateRequired()) {
                return await UpdateAndGetData(user);
            } else {
                return await GetLocalData(user);
            }
        }

        async Task<bool> IsDataUpdateRequired() {
            DateTime c = await WebService.GetLastModified(JSONFile.Categories);
            DateTime t = await WebService.GetLastModified(JSONFile.MonthlyTarget);
            DateTime a = await WebService.GetLastModified(JSONFile.MonthlyAchieved);
            DateTime lmw; // the max of three dates
            if (c > t) {
                if (c > a) {
                    lmw = c;
                } else {
                    lmw = a;
                }
            } else if (t > a) {
                lmw = t;
            } else {
                lmw = a;
            }
            DateTime lmd = await DbService.GetLastModified("data");
            if (lmw > lmd) {
                await DbService.SetLastModified("data", lmw);
                return true;
            }
            return false;
        }
        async Task<List<CollectionData>> UpdateAndGetData(User user) {
            List<JSONCategory> c = await WebService.GetData<List<JSONCategory>>(JSONFile.Categories);
            List<JSONMonthly> t = await WebService.GetData<List<JSONMonthly>>(JSONFile.MonthlyTarget);
            List<JSONMonthly> a = await WebService.GetData<List<JSONMonthly>>(JSONFile.MonthlyAchieved);

            await DbService.UpdateDBAsync(c.Select(item => new DBCategory {
                SequenceNo = item.SequenceNo,
                SlNo = item.SlNo,
                Type = item.Type
            }).ToList());
            List<DBMonthStat> mstats = new List<DBMonthStat>();
            for (int i = 0; i < t.Count; i++) {
                mstats.Add(new DBMonthStat {
                    achieved = a[i].target, target = t[i].target,
                    EmployeeCode = a[i].EmployeeCode,
                    Month = a[i].Month, Year = a[i].Year,
                    Type = a[i].Type
                });
            }
            await DbService.UpdateDBAsync(mstats);

            // Make CollectionData
            List<CollectionData> data = new List<CollectionData>();
            for (int i = 0; i < t.Count; i++) {
                if (user.Id != t[i].EmployeeCode) {
                    continue;
                }
                int type = c.Where(cat => {
                    return cat.Type.ToLower() == t[i].Type.ToLower();
                }).First().SlNo;
                CollectionData cd = new CollectionData(
                    t[i].EmployeeCode,
                    type,
                    t[i].Type,
                    t[i].target, a[i].target,
                    t[i].Year, t[i].Month);
                data.Add(cd);
            }
            return data;
        }
        async Task<List<CollectionData>> GetLocalData(User user) {
            var dbcat = await DbService.GetRowsAsync<DBCategory>();
            var dbmstat = await DbService.GetRowsAsync<DBMonthStat>(item => item.EmployeeCode == user.Id);
            return dbmstat.Select(item => new CollectionData(
                user.Id, dbcat.Where(c => c.Type.ToLower() == item.Type.ToLower()).First().SlNo,
                item.Type, item.target, item.achieved, item.Year, item.Month)).ToList();
        }
        async Task<bool> IsUsersUpdateRequired() {
            DateTime c = await WebService.GetLastModified(JSONFile.Employees);
            DateTime lmd = await DbService.GetLastModified("users");
            if (c > lmd) {
                await DbService.SetLastModified("users", c);
                return true;
            }
            return false;
        }
        async Task<List<User>> UpdateAndGetUsers() {
            List<JSONUser> c = await WebService.GetData<List<JSONUser>>(JSONFile.Employees);
            await DbService.UpdateDBAsync(c.Select(item => new DBUser {
                Id = item.Id,
                Name = item.Name
            }).ToList());
            return c.Select(item => new User(item.Id, item.Name)).ToList();
        }
        async Task<List<User>> GetLocalUsers() {
            var dbusers = await DbService.GetRowsAsync<DBUser>();
            return dbusers.Select(item => new User(item.Id, item.Name)).ToList();
        }
    }
}
