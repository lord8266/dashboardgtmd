﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DashboardGTMD
{
    interface IDataService
    {
        // Get all Users who exist in any order
         Task<List<User>> GetAllUsers();

        // Get all data available for a User in any order
         Task<List<CollectionData>> GetAllDataForUser(User user);
    }
}
