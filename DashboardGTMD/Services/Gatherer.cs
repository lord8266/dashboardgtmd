﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DashboardGTMD
{
    class Gatherer
    {
        public static List<YearlyCollectionData> GroupByYear(List<CollectionData> data)
        {
            var l = data.GroupBy(c => c.Year, (k, v) => new YearlyCollectionData(v.ToList())).ToList();
            l.Sort((yc1, yc2) => yc1.ColData[0].Year - yc2.ColData[0].Year);
            return l;
        }
        public static List<List<CollectionData>> GroupByCategory(YearlyCollectionData data)
        {
            return data.ColData.GroupBy(c => c.Id, (k, v) => v.ToList()).ToList();
        }
        public static List<List<CollectionData>> GroupByCategory(List<CollectionData> data)
        {
            return data.GroupBy(c => c.Id, (k, v) => v.ToList()).ToList();
        }
    }
}
