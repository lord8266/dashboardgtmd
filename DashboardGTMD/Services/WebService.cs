﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Xamarin.Essentials;
using System.Linq;

namespace DashboardGTMD {
    class WebService {
        static readonly HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(10) };
        static readonly NetworkAccess nAccess = Connectivity.NetworkAccess;
       
        
        public static async Task<T> GetData<T>(string url) {
            //await Task.Delay(3000); // A test to check if using cache, if call comes here delay is high
            try {
                //string url = String.Format(URLFormat, name.ToString());
                string res = await client.GetStringAsync(url);
                T d = JsonSerializer.Deserialize<T>(res);
                return d;
            }
            catch(Exception exception) {
                Console.WriteLine("Haha strong as an ox! Or Not-> {0}",exception.ToString());
                return default(T);
            }
        }

        public static async Task<DateTime> GetLastModified(string url) {
            
            try {
                var res = await client.SendAsync(new HttpRequestMessage(HttpMethod.Head, url) );
                return res.Content.Headers.LastModified.Value.DateTime;
            } catch (Exception exception) {
                Console.WriteLine("Haha strong as an ox! Or Not-> {0}", exception.ToString());
                return default(DateTime);
            }
        }

        

    }
}
