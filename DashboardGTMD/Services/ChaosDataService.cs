﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DashboardGTMD {
    class ChaosDataService : IDataService {
        int count = -1;
        public async Task<List<User>> GetAllUsers() {
            count += 1;
            await Task.Delay(1000);
            return Defaults.SampleUsers;
            if (count == 0) {
                return null;
            } else if ((new Random()).NextDouble() > 0.5) {
                return Defaults.SampleUsers;
            } else {
                return null;
            }

        }

        public async Task<List<CollectionData>> GetAllDataForUser(User user) {
            await Task.Delay(1000);
            return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
            if (count == 0) {
                return null;
            } else if ((new Random()).NextDouble() > 0.6) {
                return Defaults.SampleCollectionData.Where(c => c.UserId == user.Id).ToList();
            } else {
                return null;
            }
        }
    }
}

