﻿using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using DashboardGTMD;
using DashboardGTMD.Droid;


[assembly: ExportRenderer(typeof(Entry), typeof(HighlightEntryRenderer))]
[assembly: ExportRenderer(typeof(Picker), typeof(HighlightPickerRenderer))]
namespace DashboardGTMD.Droid {
    class HighlightEntryRenderer : EntryRenderer {
        public HighlightEntryRenderer(Context context) : base(context) {
            App.Instance.ColorChangedEvent += (s, e) => {
                SetColor(GetThemeColor());
            };
        }

        Android.Graphics.Color GetThemeColor() {

            if (Xamarin.Forms.Application.Current.UserAppTheme== OSAppTheme.Light) {
                return Android.Graphics.Color.Black;
                
            } else {
                return Android.Graphics.Color.WhiteSmoke;
            }
        }
        void SetColor(Android.Graphics.Color col) {
            Control.BackgroundTintList = ColorStateList.ValueOf(
                    col
                );
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e) {
            base.OnElementChanged(e);
            if (Control != null) {
                SetColor(GetThemeColor());
                Control.FocusChange += (sender, ee) => {
                    bool hasFocus = ee.HasFocus;
                    SetColor(GetThemeColor());
                };
            }
        }
    }

    class HighlightPickerRenderer : PickerRenderer {
        public HighlightPickerRenderer(Context context) : base(context) {
        }

        Android.Graphics.Color GetThemeColor() {

            if (Xamarin.Forms.Application.Current.UserAppTheme == OSAppTheme.Light) {
                return Android.Graphics.Color.Black;

            } else {
                return Android.Graphics.Color.WhiteSmoke;
            }
        }
        void SetColor(Android.Graphics.Color col) {
            Control.BackgroundTintList = ColorStateList.ValueOf(
                    col
                );
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e) {
            base.OnElementChanged(e);
            if (Control != null) {
                SetColor(GetThemeColor());
                Control.FocusChange += (sender, ee) => {
                    bool hasFocus = ee.HasFocus;
                    SetColor(GetThemeColor());
                };
            }
        }
    }

}